﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using webapi.Attributes;
using webapi.Datamodel;
using webapi.Models;

#if DEBUG
[assembly: log4net.Config.XmlConfigurator(Watch = true)]
#endif

namespace webapi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {

            var tokens = Request.Headers.GetValues("token");

            if (tokens != null)
            {
                var token = tokens.ToList().First();

                //string tokenUsername = TokenManager.ValidateToken(token);
                var userinfo = new BitcoinApiEntities().Users.SingleOrDefault(c => c.Token == token);

                Context.User = null;
                if (userinfo != null)
                {
                    var principalClient = new UserPrincipal(token)
                    {
                        Token = userinfo.Token,
                        UserId = userinfo.UserId,
                    };

                    Context.User = principalClient;

                }
            }
        }
    }
}

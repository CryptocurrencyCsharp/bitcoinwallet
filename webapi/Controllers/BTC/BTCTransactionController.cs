﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using NBitcoin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QBitNinja.Client;
using QBitNinja.Client.Models;
using webapi.Attributes;
using webapi.Datamodel;
using webapi.Models;

namespace webapi.Controllers.BTC
{
    [RequireAuthentication]
    public class BTCTransactionController : BaseController
    {
        //private static readonly ICoinService CoinService = new BitcoinService(useTestnet: true);
        private BitcoinApiEntities db { get; set; }

        public BTCTransactionController()
        {
            db = new BitcoinApiEntities();

        }


        // GET api/values
        [HttpGet]
        [Route("api/BTC/Transaction/Transaction/{transactionId}")]
        public ObjectResultDto<TxDto> Transaction(string transactionId, bool watchOnley = false)
        {
            //RPCClient client = new RPCClient(Network.TestNet);
            //0eab89a271380b09987bcee5258fca91f28df4dadcedf892658b9bc261050d96
            var s = "";
            using (var wc = new WebClient())
            {
                //ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                //wc.Encoding = Encoding.UTF8;
                //wc.Proxy = null;
                //myskey = myskey.Split(new string[] { "\r\n" }, StringSplitOptions.None)[0];
                s = wc.DownloadString("https://tchain.api.btc.com/v3/tx/" + transactionId + "/tx");

                var r = JsonConvert.DeserializeObject<TransactionSummeryDto>(s);
                if (r.data == null) throw new BusinessException("invalid TxId", 1254);

                return new ObjectResultDto<TxDto>
                {
                    Result = r.data
                };

            }
        }


        // GET api/values
        [HttpGet]
        [Route("api/BTC/Transaction/Transactions")]
        public object Transactions(string account = null, int pageSize = 10, int from = 0, bool watchOnley = false)
        {
            var user = db.Users.Single(w => w.UserId == CurrentUser.UserId);
            var wallet = user.Wallets.First();

            using (var wc = new WebClient())
            {
                //ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                //wc.Encoding = Encoding.UTF8;
                //wc.Proxy = null;
                //myskey = myskey.Split(new string[] { "\r\n" }, StringSplitOptions.None)[0];
                var s = wc.DownloadString("https://tchain.api.btc.com/v3/address/" + wallet.Address + "/tx");

                var r = JsonConvert.DeserializeObject<TransactionHistoryDto>(s);

                return new ObjectResultDto<PagingResult<TranactionDto>>
                {
                    Result = new PagingResult<TranactionDto>
                    {
                        Result = r == null ? new List<TranactionDto>() : r.data == null ? new List<TranactionDto>() : r.data.list.Select(w => new TranactionDto
                        {
                            TransactionId = w.hash,
                            Created = w.created_at,

                            Amount = w.balance_diff / 100000000,
                            Address = w.balance_diff > 0 ? w.inputs.SelectMany(inputDto => inputDto.prev_addresses).Where(ww => ww != wallet.Address).ToList() :
                                                            w.outputs.SelectMany(outputDto => outputDto.addresses).Where(ww => ww != wallet.Address).ToList()

                        }).OrderByDescending(o=>o.Created).ToList()
                    }
                };

            }

        }


        // POST api/values
        [HttpPost]
        [Route("api/BTC/Transaction/SendTransaction")]
        public ObjectResultDto<TranactionDto> SendTransaction([FromBody]TranactionSendDto dto)
        {

            var user = db.Users.Single(w => w.UserId == CurrentUser.UserId);

            var address = dto.AddressTo;

            //var walletFileName = string.Format(HttpContext.Current.Request.PhysicalApplicationPath + Config.DefaultWalletFileName, user.Email);

            BitcoinAddress addressToSend;
            try
            {
                addressToSend = BitcoinAddress.Create(address, Config.Network);
            }
            catch (Exception)
            {
                throw new BusinessException("AddressTo is invalid", 121);
            }

            var walletAddress = user.Wallets.Single(c => c.CurrencyType == CurrencyTypeEnum.BTC).Address;
            //var safe = Safe.Load(user.Password, walletFileName);
            var privateKey = Key.Parse(user.Privatekey, dto.Password, Config.Network);

            if (Config.ConnectionType == ConnectionType.Http)
            {
                var operationsPerAddresses = QueryOperationsPerSafeAddresses(walletAddress);

                // 1. Gather all the not empty private keys
                var operationsPerNotEmptyPrivateKeys = new Dictionary<Key, List<BalanceOperation>>();

                foreach (var elem in operationsPerAddresses)
                {
                    var balance = Money.Zero;
                    foreach (var op in elem.Value) balance += op.Amount;

                    if (balance > Money.Zero)
                    {
                        //var secret = safe.FindPrivateKey(elem.Key);
                        //var secret = safe.BitcoinExtKey;
                        //var secret = privateKey;
                        //var secret = new BitcoinExtKey(walletAddress,Config.Network);
                        
                        operationsPerNotEmptyPrivateKeys.Add(privateKey, elem.Value);
                    }
                }

                // 2. Get the script pubkey of the change.
                //Script changeScriptPubKey = null;
                //var operationsPerChangeAddresses = QueryOperationsPerSafeAddresses(safe, minUnusedKeys: 1, hdPathType: HdPathType.Change);
                //foreach (var elem in operationsPerChangeAddresses)
                //{
                //    if (elem.Value.Count == 0)
                //        changeScriptPubKey = safe.FindPrivateKey(elem.Key).ScriptPubKey;
                //}
                //if (changeScriptPubKey == null)
                //    throw new ArgumentNullException();

                // 3. Gather coins can be spend
                var unspentCoins = new Dictionary<Coin, bool>();

                var client1 = new QBitNinjaClient(Config.Network);
                var balanceModel = client1.GetBalance(new BitcoinPubKeyAddress(walletAddress), true).Result;
                //var balanceModel = client1.GetBalance(user.Wallets.First().Address, unspentOnly: false).Result;
                foreach (var operation in balanceModel.Operations)
                {
                    foreach (var elem in operation.ReceivedCoins.Select(coin => coin as Coin))
                    {
                        unspentCoins.Add(elem, operation.Confirmations > 0);
                    }
                }

                //var unspentCoins = GetUnspentCoins(operationsPerNotEmptyPrivateKeys.Keys);

                // 4. Get the fee
                Money fee;
                try
                {
                    var txSizeInBytes = 250;
                    using (var client = new HttpClient())
                    {

                        const string request = @"https://bitcoinfees.21.co/api/v1/fees/recommended";
                        var result = client.GetAsync(request, HttpCompletionOption.ResponseContentRead).Result;
                        var json = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                        var fastestSatoshiPerByteFee = json.Value<decimal>("fastestFee");
                        fee = new Money(fastestSatoshiPerByteFee * txSizeInBytes, MoneyUnit.Satoshi);
                    }
                }
                catch
                {
                    throw new BusinessException("Couldn't calculate transaction fee, try it again later.");
                }
                // WriteLine($"Fee: {fee.ToDecimal(MoneyUnit.BTC).ToString("0.#############################")}btc");

                // 5. How much money we can spend?
                var availableAmount = Money.Zero;
                var unconfirmedAvailableAmount = Money.Zero;
                foreach (var elem in unspentCoins)
                {
                    // If can spend unconfirmed add all
                    if (Config.CanSpendUnconfirmed)
                    {
                        availableAmount += elem.Key.Amount;
                        if (!elem.Value)
                            unconfirmedAvailableAmount += elem.Key.Amount;
                    }
                    // else only add confirmed ones
                    else
                    {
                        if (elem.Value)
                        {
                            availableAmount += elem.Key.Amount;
                        }
                    }
                }

                // 6. How much to spend?
                Money amountToSend = null;
                var amountString = dto.Amount;
                if (string.Equals(amountString, "all", StringComparison.OrdinalIgnoreCase))
                {
                    amountToSend = availableAmount;
                    amountToSend -= fee;
                }
                else
                {
                    amountToSend = ParseBtcString(amountString);
                }

                // 7. Do some checks
                if (amountToSend < Money.Zero || availableAmount < amountToSend + fee)
                    throw new BusinessException("Not enough coins.", 11);

                //var feePc = Math.Round((100 * fee.ToDecimal(MoneyUnit.BTC)) / amountToSend.ToDecimal(MoneyUnit.BTC));

                //var confirmedAvailableAmount = availableAmount - unconfirmedAvailableAmount;
                var totalOutAmount = amountToSend + fee;

                // 8. Select coins
                // WriteLine("Selecting coins...");
                var coinsToSpend = new HashSet<Coin>();
                var unspentConfirmedCoins = new List<Coin>();
                var unspentUnconfirmedCoins = new List<Coin>();
                foreach (var elem in unspentCoins)
                    if (elem.Value) unspentConfirmedCoins.Add(elem.Key);
                    else unspentUnconfirmedCoins.Add(elem.Key);

                var haveEnough = SelectCoins(ref coinsToSpend, totalOutAmount, unspentConfirmedCoins);
                if (!haveEnough)
                    haveEnough = SelectCoins(ref coinsToSpend, totalOutAmount, unspentUnconfirmedCoins);
                if (!haveEnough)
                    throw new Exception("Not enough funds.");

                //// 9. Get signing keys
                var signingKeys = new HashSet<Key>();

                foreach (var coin in coinsToSpend)
                {
                    foreach (var elem in operationsPerNotEmptyPrivateKeys)
                    {
                        if (elem.Key.ScriptPubKey == coin.ScriptPubKey)
                            signingKeys.Add(elem.Key);
                    }
                }


                // 10. Build the transaction
                // WriteLine("Signing transaction...");
                var builder = new TransactionBuilder();
                var tx = builder
                    .AddCoins(coinsToSpend)
                    .AddKeys(signingKeys.ToArray())
                    .Send(addressToSend, amountToSend)
                    .SetChange(privateKey.ScriptPubKey)
                    .SendFees(fee)
                    .BuildTransaction(true);

                if (!builder.Verify(tx))
                    throw new BusinessException("Couldn't build the transaction.", 111);

                // WriteLine($"Transaction Id: {tx.GetHash()}");

                var qBitClient = new QBitNinjaClient(Config.Network);

                // QBit's success response is buggy so let's check manually, too		
                BroadcastResponse broadcastResponse;
                var success = false;
                var tried = 0;
                var maxTry = 7;
                GetTransactionResponse getTxResp;
                do
                {
                    tried++;
                    // WriteLine($"Try broadcasting transaction... ({tried})");
                    broadcastResponse = qBitClient.Broadcast(tx).Result;
                    getTxResp = qBitClient.GetTransaction(tx.GetHash()).Result;
                    if (getTxResp == null)
                    {
                        Thread.Sleep(3000);
                    }
                    else
                    {
                        success = true;
                        break;
                    }
                } while (tried <= maxTry);

                if (!success)
                {
                    if (broadcastResponse.Error != null)
                        throw new BusinessException(broadcastResponse.Error.Reason, 10102);

                    throw new BusinessException("The transaction might not have been successfully broadcasted. Please check the Transaction ID in a block explorer.", 1212);
                }
                return new ObjectResultDto<TranactionDto>
                {
                    Result = new TranactionDto
                    {
                        TransactionId = getTxResp.TransactionId.ToString()
                    }
                };

            }
            if (Config.ConnectionType == ConnectionType.FullNode)
            {
                throw new NotImplementedException();
            }

            throw new BusinessException("Invalid connection type.");
        }


        private static Money ParseBtcString(string value)
        {
            decimal amount;
            if (!decimal.TryParse(
                        value.Replace(',', '.'),
                        NumberStyles.Any,
                        CultureInfo.InvariantCulture,
                        out amount))
            {
                throw new BusinessException("Wrong btc amount format.", 10);
            }


            return new Money(amount, MoneyUnit.BTC);
        }

        
        private static bool SelectCoins(ref HashSet<Coin> coinsToSpend, Money totalOutAmount, List<Coin> unspentCoins)
        {
            var haveEnough = false;
            foreach (var coin in unspentCoins.OrderByDescending(x => x.Amount))
            {
                coinsToSpend.Add(coin);
                // if doesn't reach amount, continue adding next coin
                if (coinsToSpend.Sum(x => x.Amount) < totalOutAmount) continue;
                haveEnough = true;
                break;
            }

            return haveEnough;
        }
        

        private static Dictionary<BitcoinAddress, List<BalanceOperation>> QueryOperationsPerSafeAddresses(string address)
        {
            //if (hdPathType == null)
            //{
            //    var operationsPerReceiveAddresses = QueryOperationsPerSafeAddresses(safe, 7, HdPathType.Receive);
            //    var operationsPerChangeAddresses = QueryOperationsPerSafeAddresses(safe, 7, HdPathType.Change);

            //    var operationsPerAllAddresses = new Dictionary<BitcoinAddress, List<BalanceOperation>>();

            //    foreach (var elem in operationsPerReceiveAddresses)
            //        operationsPerAllAddresses.Add(elem.Key, elem.Value);

            //    foreach (var elem in operationsPerChangeAddresses)
            //        operationsPerAllAddresses.Add(elem.Key, elem.Value);

            //    return operationsPerAllAddresses;
            //}

            //var addresses = safe.GetFirstNAddresses(minUnusedKeys, hdPathType.GetValueOrDefault());
            var addresses = new List<BitcoinAddress>
            {
                BitcoinAddress.Create(address,Config.Network)
            };

            //var addresses = FakeData.FakeSafe.GetFirstNAddresses(minUnusedKeys);

            var operationsPerAddresses = new Dictionary<BitcoinAddress, List<BalanceOperation>>();
            var unusedKeyCount = 0;
            foreach (var elem in QueryOperationsPerAddresses(addresses))
            {
                operationsPerAddresses.Add(elem.Key, elem.Value);
                if (elem.Value.Count == 0) unusedKeyCount++;
            }
            // WriteLine(($"{operationsPerAddresses.Count} {hdPathType} keys are processed.");

            //var startIndex = minUnusedKeys;
            //while (unusedKeyCount < minUnusedKeys)
            //{
            //    addresses = new List<BitcoinAddress>();
            //    for (var i = startIndex; i < startIndex + minUnusedKeys; i++)
            //    {
            //        addresses.Add(safe.GetAddress(i, hdPathType.GetValueOrDefault()));
            //        //addresses.Add(FakeData.FakeSafe.GetAddress(i));
            //    }
            //    foreach (var elem in QueryOperationsPerAddresses(addresses))
            //    {
            //        operationsPerAddresses.Add(elem.Key, elem.Value);
            //        if (elem.Value.Count == 0) unusedKeyCount++;
            //    }
            //    // WriteLine(($"{operationsPerAddresses.Count} {hdPathType} keys are processed.");
            //    startIndex += minUnusedKeys;
            //}

            return operationsPerAddresses;
        }

        private static Dictionary<BitcoinAddress, List<BalanceOperation>> QueryOperationsPerAddresses(IEnumerable<BitcoinAddress> addresses)
        {
            var operationsPerAddresses = new Dictionary<BitcoinAddress, List<BalanceOperation>>();
            var client = new QBitNinjaClient(Config.Network);
            foreach (var addr in addresses)
            {
                var operations = client.GetBalance(addr, unspentOnly: false).Result.Operations;
                operationsPerAddresses.Add(addr, operations);
            }
            return operationsPerAddresses;
        }
    }
}

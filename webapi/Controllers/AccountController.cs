﻿using System;
using System.Linq;
using System.Web.Http;
using NBitcoin;
using Nethereum.Hex.HexConvertors.Extensions;
using Nethereum.Signer;
using webapi.Datamodel;
using webapi.Models;

namespace webapi.Controllers
{
    public class AccountController : ApiController
    {
        //private static readonly ICoinService CoinService = new BitcoinService(useTestnet: true);
        private BitcoinApiEntities db { get; set; }

        public AccountController()
        {
            db = new BitcoinApiEntities();
        }


        [HttpPost]
        [Route("api/Account/Login")]
        public ObjectResultDto<string> Login(UserDto user)
        {
            var u = db.Users.SingleOrDefault(c => c.WalletId == user.WalletId);

            if (u == null)
                throw new BusinessException("The user was not found.", 4);

            bool credentials = u.Password.Equals(user.Password);

            if (!credentials)
                throw new BusinessException("The username/password combination was wrong.", 5);

            //var token = TokenManager.GenerateToken(user.Email);
            var token = CommonUtility.GetReferenceNumber();
            u.Token = token;

            db.SaveChanges();

            return new ObjectResultDto<string> { Result = token };
        }


        [HttpPost]
        [Route("api/Account/SignUp")]
        public ObjectResultDto<SignUpResponseDto> SignUp(UserSignUpDto dto)
        {
            if (!ModelState.IsValid)
                throw new BusinessException("Input data IS invalid", 1);

            if (string.IsNullOrEmpty(dto.Email))
                throw new BusinessException("The Email Empty.", 1);
            //Request.CreateResponse(HttpStatusCode.BadRequest, );

            var user = db.Users.SingleOrDefault(c => c.Email == dto.Email);

            if (user != null)
                throw new BusinessException("The user was found.", 2);


            //var walletFileName = "";
            User u;

            try
            {

                var extKey = new ExtKey();

                BitcoinAddress address = extKey.PrivateKey.PubKey.GetAddress(Config.Network);

                var privateKeyEncrypted = extKey.PrivateKey.GetEncryptedBitcoinSecret(dto.Password, Config.Network).ToWif();
                //var rndKey = System.Text.Encoding.UTF8.GetString(mnemo.DeriveSeed());

                u = db.Users.Add(new User
                {
                    Email = dto.Email,
                    Password = dto.Password,
                    //Entrope = Convert.ToBase64String(extKey.ChainCode),
                    Privatekey = privateKeyEncrypted,
                    WalletId = CommonUtility.GetReferenceNumber(),
                });

                u.Wallets.Add(new Wallet
                {
                    CurrencyType = CurrencyTypeEnum.BTC,
                    Label = "BitCoin1",
                    Address = address.ToString(),                    
                    PubKey = extKey.PrivateKey.PubKey.ToString(),
                    ScriptPubKey = extKey.PrivateKey.ScriptPubKey.ToHex(),

                });

                var key = new EthECKey(extKey.PrivateKey.ToBytes(), true);

                u.Wallets.Add(new Wallet
                {
                    CurrencyType = CurrencyTypeEnum.ETH,
                    Label = "Etherum",
                    Address = key.GetPublicAddress(),
                    PubKey = key.GetPubKey().ToHex(),
                    ScriptPubKey = null,
                });

                db.SaveChanges();
            }
            catch (Exception)
            {
                //File.Delete(walletFileName);
                throw;
            }

            return new ObjectResultDto<SignUpResponseDto>
            {
                Result = new SignUpResponseDto
                {
                    WalletId = u.WalletId
                }

            };
        }


        //[HttpGet]
        //public HttpResponseMessage Validate(string token, string email)
        //{
        //    var exists = new BitcoinApiEntities().Users.SingleOrDefault(c => c.Email == email);

        //    if (exists == null) return Request.CreateResponse(HttpStatusCode.NotFound, "The user was not found.");

        //    string tokenUsername = TokenManager.ValidateToken(token);

        //    if (email.Equals(tokenUsername))
        //        return Request.CreateResponse(HttpStatusCode.OK);

        //    return Request.CreateResponse(HttpStatusCode.BadRequest);
        //}

    }
}

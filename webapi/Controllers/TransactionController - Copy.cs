﻿//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.IO;
//using System.Linq;
//using System.Net.Http;
//using System.Text;
//using System.Threading;
//using System.Web;
//using System.Web.Http;
//using BitcoinLib.Services.Coins.Base;
//using BitcoinLib.Services.Coins.Bitcoin;
//using DotNetWallet;
//using HBitcoin.KeyManagement;
//using NBitcoin;
//using Newtonsoft.Json.Linq;
//using QBitNinja.Client;
//using QBitNinja.Client.Models;
//using webapi.Attributes;
//using webapi.Datamodel;
//using webapi.Models;
//using webapi.QBitNinjaJutsus;

//namespace webapi.Controllers
//{
//    [RequireAuthentication]
//    public class TestTransactionController : BaseController
//    {
//        private static readonly ICoinService CoinService = new BitcoinService(useTestnet: true);
//        private BitcoinApiEntities db { get; set; }

//        public TransactionController()
//        {
//            db = new BitcoinApiEntities();

//        }


//        // GET api/values
//        [HttpGet]
//        public object Transaction(string tansactionId, bool watchOnley = false)
//        {
//            //RPCClient client = new RPCClient(Network.TestNet);


//            return new ObjectResultDto<TranactionDto>
//            {
//                Result = new TranactionDto
//                {
//                }
//            };
//        }


//        // GET api/values
//        [HttpGet]
//        public object Transactions(string account = null, int pageSize = 10, int from = 0, bool watchOnley = false)
//        {
//            //RPCClient client = new RPCClient(Network.TestNet);
//            var user = db.Users.Single(w => w.UserId == CurrentUser.UserId);
//            var wallet = user.Wallets.First();
//            var bitcoinPrivateKey = new BitcoinSecret(user.Privatekey);

//            return new ObjectResultDto<PagingResult<TranactionDto>>
//            {
//                Result = new PagingResult<TranactionDto>()
//                {
//                    Result = new List<TranactionDto>()
//                }
//            };
//        }


//        // POST api/values
//        [HttpPost]
//        public ObjectResultDto<TranactionDto> SendTransaction([FromBody]TranactionSendDto dto)
//        {

//            var user = db.Users.Single(w => w.UserId == CurrentUser.UserId);

//            var address = dto.AddressTo;

//            var walletFileName = string.Format(HttpContext.Current.Request.PhysicalApplicationPath + Config.DefaultWalletFileName, user.Email);

//            var addressToSend = BitcoinAddress.Create(address, Config.Network);

//            var safe =  Safe.Load(user.Password, walletFileName);

//            if (Config.ConnectionType == ConnectionType.Http)
//            {
//                var operationsPerAddresses = QueryOperationsPerSafeAddresses(safe, 7);

//                // 1. Gather all the not empty private keys
//                var operationsPerNotEmptyPrivateKeys = new Dictionary<BitcoinExtKey, List<BalanceOperation>>();
//                foreach (var elem in operationsPerAddresses)
//                {
//                    var balance = Money.Zero;
//                    foreach (var op in elem.Value) balance += op.Amount;

//                    if (balance > Money.Zero)
//                    {
//                        var secret = safe.FindPrivateKey(elem.Key);
//                        operationsPerNotEmptyPrivateKeys.Add(secret, elem.Value);
//                    }
//                }

//                // 2. Get the script pubkey of the change.
//                Script changeScriptPubKey = null;
//                var operationsPerChangeAddresses = QueryOperationsPerSafeAddresses(safe, minUnusedKeys: 1, hdPathType: HdPathType.Change);
//                foreach (var elem in operationsPerChangeAddresses)
//                {
//                    if (elem.Value.Count == 0)
//                        changeScriptPubKey = safe.FindPrivateKey(elem.Key).ScriptPubKey;
//                }
//                if (changeScriptPubKey == null)
//                    throw new ArgumentNullException();

//                // 3. Gather coins can be spend
//                var unspentCoins = GetUnspentCoins(operationsPerNotEmptyPrivateKeys.Keys);

//                // 4. Get the fee
//                Money fee;
//                try
//                {
//                    var txSizeInBytes = 250;
//                    using (var client = new HttpClient())
//                    {

//                        const string request = @"https://bitcoinfees.21.co/api/v1/fees/recommended";
//                        var result = client.GetAsync(request, HttpCompletionOption.ResponseContentRead).Result;
//                        var json = JObject.Parse(result.Content.ReadAsStringAsync().Result);
//                        var fastestSatoshiPerByteFee = json.Value<decimal>("fastestFee");
//                        fee = new Money(fastestSatoshiPerByteFee * txSizeInBytes, MoneyUnit.Satoshi);
//                    }
//                }
//                catch
//                {
//                    throw new BusinessException("Couldn't calculate transaction fee, try it again later.");
//                    throw new Exception("Can't get tx fee");
//                }
//                // WriteLine($"Fee: {fee.ToDecimal(MoneyUnit.BTC).ToString("0.#############################")}btc");

//                // 5. How much money we can spend?
//                var availableAmount = Money.Zero;
//                var unconfirmedAvailableAmount = Money.Zero;
//                foreach (var elem in unspentCoins)
//                {
//                    // If can spend unconfirmed add all
//                    if (Config.CanSpendUnconfirmed)
//                    {
//                        availableAmount += elem.Key.Amount;
//                        if (!elem.Value)
//                            unconfirmedAvailableAmount += elem.Key.Amount;
//                    }
//                    // else only add confirmed ones
//                    else
//                    {
//                        if (elem.Value)
//                        {
//                            availableAmount += elem.Key.Amount;
//                        }
//                    }
//                }

//                // 6. How much to spend?
//                Money amountToSend = null;
//                var amountString = dto.Amount;
//                if (string.Equals(amountString, "all", StringComparison.OrdinalIgnoreCase))
//                {
//                    amountToSend = availableAmount;
//                    amountToSend -= fee;
//                }
//                else
//                {
//                    amountToSend = ParseBtcString(amountString);
//                }

//                // 7. Do some checks
//                if (amountToSend < Money.Zero || availableAmount < amountToSend + fee)
//                    throw new BusinessException("Not enough coins.", 11);

//                var feePc = Math.Round((100 * fee.ToDecimal(MoneyUnit.BTC)) / amountToSend.ToDecimal(MoneyUnit.BTC));

//                var confirmedAvailableAmount = availableAmount - unconfirmedAvailableAmount;
//                var totalOutAmount = amountToSend + fee;

//                // 8. Select coins
//                // WriteLine("Selecting coins...");
//                var coinsToSpend = new HashSet<Coin>();
//                var unspentConfirmedCoins = new List<Coin>();
//                var unspentUnconfirmedCoins = new List<Coin>();
//                foreach (var elem in unspentCoins)
//                    if (elem.Value) unspentConfirmedCoins.Add(elem.Key);
//                    else unspentUnconfirmedCoins.Add(elem.Key);

//                var haveEnough = SelectCoins(ref coinsToSpend, totalOutAmount, unspentConfirmedCoins);
//                if (!haveEnough)
//                    haveEnough = SelectCoins(ref coinsToSpend, totalOutAmount, unspentUnconfirmedCoins);
//                if (!haveEnough)
//                    throw new Exception("Not enough funds.");

//                // 9. Get signing keys
//                var signingKeys = new HashSet<ISecret>();
//                foreach (var coin in coinsToSpend)
//                {
//                    foreach (var elem in operationsPerNotEmptyPrivateKeys)
//                    {
//                        if (elem.Key.ScriptPubKey == coin.ScriptPubKey)
//                            signingKeys.Add(elem.Key);
//                    }
//                }

//                // 10. Build the transaction
//                // WriteLine("Signing transaction...");
//                var builder = new TransactionBuilder();
//                var tx = builder
//                    .AddCoins(coinsToSpend)
//                    .AddKeys(signingKeys.ToArray())
//                    .Send(addressToSend, amountToSend)
//                    .SetChange(changeScriptPubKey)
//                    .SendFees(fee)
//                    .BuildTransaction(true);

//                QBitNinjaClient qBitClient;

//                if (!builder.Verify(tx))
//                    throw new BusinessException("Couldn't build the transaction.",111);

//                    // WriteLine($"Transaction Id: {tx.GetHash()}");

//                qBitClient = new QBitNinjaClient(Config.Network);

//                // QBit's success response is buggy so let's check manually, too		
//                BroadcastResponse broadcastResponse;
//                var success = false;
//                var tried = 0;
//                var maxTry = 7;
//                do
//                {
//                    tried++;
//                    // WriteLine($"Try broadcasting transaction... ({tried})");
//                    broadcastResponse = qBitClient.Broadcast(tx).Result;
//                    var getTxResp = qBitClient.GetTransaction(tx.GetHash()).Result;
//                    if (getTxResp == null)
//                    {
//                        Thread.Sleep(3000);
//                        continue;
//                    }
//                    else
//                    {
//                        success = true;
//                        break;
//                    }
//                } while (tried <= maxTry);
//                if (!success)
//                {
//                    if (broadcastResponse.Error != null)
//                    {
//                        // WriteLine($"Error code: {broadcastResponse.Error.ErrorCode} Reason: {broadcastResponse.Error.Reason}");
//                    }
//                    throw new BusinessException("The transaction might not have been successfully broadcasted. Please check the Transaction ID in a block explorer.", 1212);
//                }
//                throw new BusinessException("Transaction is successfully propagated on the network.", 1112);
//            }
//            else if (Config.ConnectionType == ConnectionType.FullNode)
//            {
//                throw new NotImplementedException();
//            }
//            else
//            {
//                throw new BusinessException("Invalid connection type.");
//            }


//            return new ObjectResultDto<TranactionDto>
//            {
//                Result = new TranactionDto()
//            };
//        }


//        #region CommandLineArgumentStuff

//        private static string GetWalletFilePath(string email)
//        {

//            var walletFileName = string.Format(Config.DefaultWalletFileName, email);
            
//            var walletDirName = "Wallets";
//            Directory.CreateDirectory(walletDirName);
//            return Path.Combine(walletDirName, walletFileName);
//        }

//        #endregion


//        private static Money ParseBtcString(string value)
//        {
//            decimal amount;
//            if (!decimal.TryParse(
//                        value.Replace(',', '.'),
//                        NumberStyles.Any,
//                        CultureInfo.InvariantCulture,
//                        out amount))
//            {
//                throw new BusinessException("Wrong btc amount format.", 10);
//            }


//            return new Money(amount, MoneyUnit.BTC);
//        }

//        public static void GetBalances(IEnumerable<AddressHistoryRecord> addressHistoryRecords, out Money confirmedBalance, out Money unconfirmedBalance)
//        {
//            confirmedBalance = Money.Zero;
//            unconfirmedBalance = Money.Zero;
//            foreach (var record in addressHistoryRecords)
//            {
//                if (record.Confirmed)
//                    confirmedBalance += record.Amount;
//                else
//                {
//                    unconfirmedBalance += record.Amount;
//                }
//            }
//        }
//        public static bool SelectCoins(ref HashSet<Coin> coinsToSpend, Money totalOutAmount, List<Coin> unspentCoins)
//        {
//            var haveEnough = false;
//            foreach (var coin in unspentCoins.OrderByDescending(x => x.Amount))
//            {
//                coinsToSpend.Add(coin);
//                // if doesn't reach amount, continue adding next coin
//                if (coinsToSpend.Sum(x => x.Amount) < totalOutAmount) continue;
//                else
//                {
//                    haveEnough = true;
//                    break;
//                }
//            }

//            return haveEnough;
//        }
//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="secrets"></param>
//        /// <returns>dictionary with coins and if confirmed</returns>
//        public static Dictionary<Coin, bool> GetUnspentCoins(IEnumerable<ISecret> secrets)
//        {
//            var unspentCoins = new Dictionary<Coin, bool>();
//            foreach (var secret in secrets)
//            {
//                var destination = secret.PrivateKey.ScriptPubKey.GetDestinationAddress(Config.Network);

//                var client = new QBitNinjaClient(Config.Network);
//                var balanceModel = client.GetBalance(destination, unspentOnly: true).Result;
//                foreach (var operation in balanceModel.Operations)
//                {
//                    foreach (var elem in operation.ReceivedCoins.Select(coin => coin as Coin))
//                    {
//                        unspentCoins.Add(elem, operation.Confirmations > 0);
//                    }
//                }
//            }

//            return unspentCoins;
//        }

//        public static Dictionary<uint256, List<BalanceOperation>> GetOperationsPerTransactions(Dictionary<BitcoinAddress, List<BalanceOperation>> operationsPerAddresses)
//        {
//            // 1. Get all the unique operations
//            var opSet = new HashSet<BalanceOperation>();
//            foreach (var elem in operationsPerAddresses)
//                foreach (var op in elem.Value)
//                    opSet.Add(op);

//            if (opSet.Count() == 0) throw new BusinessException("Wallet has no history yet.", 20);

//            // 2. Get all operations, grouped by transactions
//            var operationsPerTransactions = new Dictionary<uint256, List<BalanceOperation>>();
//            foreach (var op in opSet)
//            {
//                var txId = op.TransactionId;
//                List<BalanceOperation> ol;
//                if (operationsPerTransactions.TryGetValue(txId, out ol))
//                {
//                    ol.Add(op);
//                    operationsPerTransactions[txId] = ol;
//                }
//                else operationsPerTransactions.Add(txId, new List<BalanceOperation> { op });
//            }

//            return operationsPerTransactions;
//        }

//        public static Dictionary<BitcoinAddress, List<BalanceOperation>> QueryOperationsPerSafeAddresses(Safe safe, int minUnusedKeys = 7, HdPathType? hdPathType = null)
//        {
//            if (hdPathType == null)
//            {
//                var operationsPerReceiveAddresses = QueryOperationsPerSafeAddresses(safe, 7, HdPathType.Receive);
//                var operationsPerChangeAddresses = QueryOperationsPerSafeAddresses(safe, 7, HdPathType.Change);

//                var operationsPerAllAddresses = new Dictionary<BitcoinAddress, List<BalanceOperation>>();

//                foreach (var elem in operationsPerReceiveAddresses)
//                    operationsPerAllAddresses.Add(elem.Key, elem.Value);

//                foreach (var elem in operationsPerChangeAddresses)
//                    operationsPerAllAddresses.Add(elem.Key, elem.Value);

//                return operationsPerAllAddresses;
//            }

//            var addresses = safe.GetFirstNAddresses(minUnusedKeys, hdPathType.GetValueOrDefault());
//            //var addresses = FakeData.FakeSafe.GetFirstNAddresses(minUnusedKeys);

//            var operationsPerAddresses = new Dictionary<BitcoinAddress, List<BalanceOperation>>();
//            var unusedKeyCount = 0;
//            foreach (var elem in QueryOperationsPerAddresses(addresses))
//            {
//                operationsPerAddresses.Add(elem.Key, elem.Value);
//                if (elem.Value.Count == 0) unusedKeyCount++;
//            }
//            // WriteLine(($"{operationsPerAddresses.Count} {hdPathType} keys are processed.");

//            var startIndex = minUnusedKeys;
//            while (unusedKeyCount < minUnusedKeys)
//            {
//                addresses = new List<BitcoinAddress>();
//                for (var i = startIndex; i < startIndex + minUnusedKeys; i++)
//                {
//                    addresses.Add(safe.GetAddress(i, hdPathType.GetValueOrDefault()));
//                    //addresses.Add(FakeData.FakeSafe.GetAddress(i));
//                }
//                foreach (var elem in QueryOperationsPerAddresses(addresses))
//                {
//                    operationsPerAddresses.Add(elem.Key, elem.Value);
//                    if (elem.Value.Count == 0) unusedKeyCount++;
//                }
//                // WriteLine(($"{operationsPerAddresses.Count} {hdPathType} keys are processed.");
//                startIndex += minUnusedKeys;
//            }

//            return operationsPerAddresses;
//        }

//        public static Dictionary<BitcoinAddress, List<BalanceOperation>> QueryOperationsPerAddresses(IEnumerable<BitcoinAddress> addresses)
//        {
//            var operationsPerAddresses = new Dictionary<BitcoinAddress, List<BalanceOperation>>();
//            var client = new QBitNinjaClient(Config.Network);
//            foreach (var addr in addresses)
//            {
//                var operations = client.GetBalance(addr, unspentOnly: false).Result.Operations;
//                operationsPerAddresses.Add(addr, operations);
//            }
//            return operationsPerAddresses;
//        }
//    }
//}

﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using NBitcoin;
using QBitNinja.Client;
using webapi.Attributes;
using webapi.Datamodel;
using webapi.Models;

namespace webapi.Controllers.BTH
{
    [RequireAuthentication]
    public class BTHWalletController : BaseController
    {
        //private static readonly ICoinService CoinService = new BitcoinService(useTestnet: true);
        private BitcoinApiEntities Db { get; set; }

        public BTHWalletController()
        {
            Db = new BitcoinApiEntities();
        }

        [Route("api/BTH/Wallet/Address/{addresss}")]
        [HttpGet]
        public string Address(string address)
        {
            return "value";
        }

        [Route("api/BTH/Wallet/Balance/{address}")]
        [HttpGet]
        public object Balance(string address)
        {
            var ssBalance = SsBalance(address);

            return new ObjectResultDto<string>
            {
                Result = ssBalance.ToString()
            };
        }

        private decimal SsBalance(string address)
        {
            
            var user = Db.Users.Single(w => w.UserId == CurrentUser.UserId);

            if (!user.Wallets.Any(w => w.Address == address && w.CurrencyType == CurrencyTypeEnum.BTH))
                throw new BusinessException("Address not valid", 121);

            var client = new QBitNinjaClient(Config.Network);
            var balance = client.GetBalance(new BitcoinPubKeyAddress(address), true).Result;

            var ssBalance = 0.0M;
            //var ssConfirmedBalance = 0.0M;
            //if (balance.Operations.Count > 0)
            //{
            //    var unspentCoins = new List<Coin>();
            //    var unspentCoinsConfirmed = new List<Coin>();
            //    foreach (var operation in balance.Operations)
            //    {
            //        unspentCoins.AddRange(operation.ReceivedCoins.Select(coin => coin as Coin));
            //        if (operation.Confirmations > 0)
            //            unspentCoinsConfirmed.AddRange(operation.ReceivedCoins.Select(coin => coin as Coin));
            //    }
            //    ssBalance = unspentCoins.Sum(x => x.Amount.ToDecimal(MoneyUnit.BTH));
            //    ssConfirmedBalance = unspentCoinsConfirmed.Sum(x => x.Amount.ToDecimal(MoneyUnit.BTH));
            //}
            return ssBalance;
        }

    }
}

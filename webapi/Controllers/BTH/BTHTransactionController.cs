﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

using NBitcoin;
using webapi.Attributes;
using webapi.Datamodel;
using webapi.Models;

namespace webapi.Controllers.BTH
{
    [RequireAuthentication]
    public class BTHTransactionController : BaseController
    {
        //private static readonly ICoinService CoinService = new BitcoinService(useTestnet: true);
        private BitcoinApiEntities db { get; set; }

        public BTHTransactionController()
        {
            db = new BitcoinApiEntities();

        }


        // GET api/values
        [HttpGet]
        [Route("api/BTH/Transaction/Transaction/{transactionId}")]
        public ObjectResultDto<TxDto> Transaction(string transactionId, bool watchOnley = false)
        {
            var s = "";
            using (var wc = new WebClient())
            {

                //s = wc.DownloadString("https://tchain.api.btc.com/v3/tx/" + transactionId + "/tx");

                //var r = JsonConvert.DeserializeObject<TransactionSummeryDto>(s);
                //if (r.data == null) throw new BusinessException("invalid TxId", 1254);

                return new ObjectResultDto<TxDto>
                {
                    //Result = r.data
                    Result = new TxDto()
                };

            }
        }


        // GET api/values
        [HttpGet]
        [Route("api/BTH/Transaction/Transactions")]
        public object Transactions(string account = null, int pageSize = 10, int from = 0, bool watchOnley = false)
        {
            var user = db.Users.Single(w => w.UserId == CurrentUser.UserId);
            var wallet = user.Wallets.First();

            using (var wc = new WebClient())
            {

                //var s = wc.DownloadString("https://tchain.api.btc.com/v3/address/" + wallet.Address + "/tx");

                //var r = JsonConvert.DeserializeObject<TransactionHistoryDto>(s);

                var r = new TransactionHistoryDto();

                return new ObjectResultDto<PagingResult<TranactionDto>>
                {
                    Result = new PagingResult<TranactionDto>
                    {
                        Result = r == null ? new List<TranactionDto>() : r.data == null ? new List<TranactionDto>() : r.data.list.Select(w => new TranactionDto
                        {
                            TransactionId = w.hash,
                            Created = w.created_at,

                            Amount = w.balance_diff / 100000000,
                            Address = w.balance_diff > 0 ? w.inputs.SelectMany(inputDto => inputDto.prev_addresses).Where(ww => ww != wallet.Address).ToList() :
                                                            w.outputs.SelectMany(outputDto => outputDto.addresses).Where(ww => ww != wallet.Address).ToList()

                        }).OrderByDescending(o => o.Created).ToList()
                    }
                };

            }

        }


        // POST api/values
        [HttpPost]
        [Route("api/BTH/Transaction/SendTransaction")]
        public ObjectResultDto<TranactionDto> SendTransaction([FromBody]TranactionSendDto dto)
        {

            var user = db.Users.Single(w => w.UserId == CurrentUser.UserId);

            var address = dto.AddressTo;

            var walletFileName = string.Format(HttpContext.Current.Request.PhysicalApplicationPath + Config.DefaultWalletFileName, user.Email);

            BitcoinAddress addressToSend;
            try
            {
                addressToSend = BitcoinAddress.Create(address, Config.Network);
            }
            catch (Exception)
            {
                throw new BusinessException("AddressTo is invalid", 121);
            }



            return new ObjectResultDto<TranactionDto>
            {
                Result = new TranactionDto
                {
                    TransactionId = ""
                }
            };

            throw new BusinessException("Invalid connection type.");
        }
    }
}

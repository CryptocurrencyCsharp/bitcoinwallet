﻿using System.Web;
using System.Web.Http;
using webapi.Attributes;

namespace webapi.Controllers
{
    public class BaseController : ApiController
    {
        public UserPrincipal CurrentUser => HttpContext.Current.User as UserPrincipal;
    }
}
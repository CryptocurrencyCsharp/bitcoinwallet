﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Bitcoin.BIP39;
using NBitcoin;
using Nethereum.Hex.HexConvertors.Extensions;
using Nethereum.Signer;
using webapi.Attributes;
using webapi.Datamodel;
using webapi.Models;

namespace webapi.Controllers
{
    [RequireAuthentication]
    public class WalletController : BaseController
    {
        private BitcoinApiEntities Db { get; set; }

        public WalletController()
        {
            Db = new BitcoinApiEntities();
        }

        [Route("api/Wallet/Restore")]
        [HttpPost]
        [AllowAnonymous]
        public ObjectResultDto<string> Restore(WalletRestoreDto dto)
        {
            var user = Db.Users.SingleOrDefault(w => w.Email == dto.Email);
            if (user != null)
            {
                throw new BusinessException("Email is exist", 104);
            }

            var bip39Frommnemonic = new BIP39(dto.mnomenic, dto.Password,BIP39.Language.English);

            //var mnemonic = new Mnemonic(Wordlist.English, bip39frommnemonic.EntropyBytes);
            //var deriveExtKey = mnemonic.DeriveExtKey(dto.Password);
            //var privateKey = deriveExtKey.PrivateKey;


            var privateKey = new Key(bip39Frommnemonic.EntropyBytes);

            var privateKeyEncrypted = privateKey.GetEncryptedBitcoinSecret(dto.Password, Config.Network).ToWif();
            
            //var privateKey = k;
            //var rndKey = System.Text.Encoding.UTF8.GetString(mnemonic.DeriveSeed());

            BitcoinAddress address = privateKey.PubKey.GetAddress(Config.Network);

            var u = Db.Users.Add(new User
            {
                Email = dto.Email,
                Password = dto.Password,
                Entrope = null,
                Privatekey = privateKeyEncrypted,
                WalletId = CommonUtility.GetReferenceNumber(),
            });

            u.Wallets.Add(new Wallet
            {
                CurrencyType = "BTC",
                Label = "BitCoin1",
                Address = address.ToString(),
                PubKey = privateKey.PubKey.ToString(),
                //ScriptPubKey = safe.BitcoinExtKey.ScriptPubKey.ToHex(),
                ScriptPubKey = privateKey.ScriptPubKey.ToHex(),
            });

            var key = new EthECKey(privateKey.ToBytes(), true);

            u.Wallets.Add(new Wallet
            {
                CurrencyType = CurrencyTypeEnum.ETH,
                Label = "Etherum",
                Address = key.GetPublicAddress(),
                PubKey = key.GetPubKey().ToHex(),
                ScriptPubKey = null,
            });

            Db.SaveChanges();

            return new ObjectResultDto<string>
            {
                Result = u.WalletId
            };
        }

        [Route("api/Wallet/Backup/")]
        [HttpPost]
        public ObjectResultDto<string> Backup(Backupdto dto)
        {
            var user = Db.Users.SingleOrDefault(w => w.UserId == CurrentUser.UserId && w.Password == dto.Password);
            if (user == null)
                throw new BusinessException("User not valid", 121);

            //var chainCode = Convert.FromBase64String(user.Entrope);

            var privateKey = Key.Parse(user.Privatekey, dto.Password, Config.Network);
            var privateKeyEncrypted = privateKey.GetEncryptedBitcoinSecret(dto.Password, Config.Network).ToWif();

            //var extKey = new ExtKey(privateKey,chainCode);

            //var entropy = System.Text.Encoding.UTF8.GetBytes(SecurityUtility.Decrypt(user.Entrope , privateKeyEncrypted)).Take(32).ToArray();
            //var entropyBytes = System.Text.Encoding.UTF8.GetBytes(privateKeyEncrypted);
            //var entropyBytes = Utilities.HexStringToBytes(privateKeyEncrypted.Substring(0,32));
            //var entropyBytes = chainCode;
            var entropyBytes = privateKey.ToBytes();

            var bip39 = new BIP39(entropyBytes, dto.Password);

            //var mnemonic = new Mnemonic(Wordlist.English, entropy);

            return new ObjectResultDto<string>
            {
                Result = bip39.MnemonicSentence
            };
        } 


        [Route("api/Wallet/Addresses")]
        [HttpGet]
        public ObjectResultDto<List<AddressDto>> Addresses()
        {
            var user = Db.Users.Single(w => w.UserId == CurrentUser.UserId);

            var wallet = user.Wallets;

            var result = new ObjectResultDto<List<AddressDto>>
            {
                Result = wallet.Select(c => new AddressDto
                {
                    Address = c.Address,
                    CurrencyType = c.CurrencyType,
                    Label = c.Label,
                    IsArchive = c.IsArchive,
                }).ToList()
            };

            result.Result.Add(new AddressDto
            {
                Address = "DdGsQTasadDFiFGHueDrwe",
                CurrencyType = "BCH",
                Label = "",
                IsArchive = false,
            });

            return result;
        }

        //// POST api/values
        //[HttpPost]
        //public object NewAddress([FromBody]string label)
        //{
        //    var bitcoinApiEntities = new BitcoinApiEntities();

        //    return CoinService.GetNewAddress(label);

        //}
    }
}

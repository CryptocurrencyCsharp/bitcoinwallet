﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Nethereum.Web3;
using webapi.Attributes;
using webapi.Datamodel;
using webapi.Models;

namespace webapi.Controllers.ETH
{
    [RequireAuthentication]
    public class ETHWalletController : BaseController
    {
        //private static readonly ICoinService CoinService = new BitcoinService(useTestnet: true);
        private BitcoinApiEntities Db { get; set; }

        public ETHWalletController()
        {
            Db = new BitcoinApiEntities();
        }

        [Route("api/ETH/Wallet/Address/{addresss}")]
        [HttpGet]
        public string Address(string address)
        {
            return "value";
        }

        [Route("api/ETH/Wallet/Balance/{address}")]
        [HttpGet]
        public async Task<ObjectResultDto<string>> Balance(string address)
        {
            var user = Db.Users.Single(w => w.UserId == CurrentUser.UserId);

            if (!user.Wallets.Any(w => w.Address == address && w.CurrencyType == CurrencyTypeEnum.ETH))
                throw new BusinessException("Address not valid", 121);

            var web3 = GetWeb3();
            var weiBalance = await web3.Eth.GetBalance.SendRequestAsync(address).ConfigureAwait(false);

            return new ObjectResultDto<string>
            {
                Result = Web3.Convert.FromWei(weiBalance).ToString()
            };
        }

        [Route("api/ETH/Wallet/Account/{address}")]
        [HttpGet]
        public string Account(string address)
        {
            //return CoinService.GetAccount(address);
            return "";
        }


        private Web3 GetWeb3()
        {
            return new Web3(Config.ETHClientUrl);
        }
    }

}

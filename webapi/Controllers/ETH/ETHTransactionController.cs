﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using NBitcoin;
using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Signer;
using Nethereum.Util;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QBitNinja.Client;
using QBitNinja.Client.Models;
using webapi.Attributes;
using webapi.Datamodel;
using webapi.Models;
using Nethereum.StandardTokenEIP20.CQS;

namespace webapi.Controllers.ETH
{
    [RequireAuthentication]
    public class ETHTransactionController : BaseController
    {
        //private static readonly ICoinService CoinService = new BitcoinService(useTestnet: true);
        private BitcoinApiEntities db { get; set; }

        public ETHTransactionController()
        {
            db = new BitcoinApiEntities();
        }

        // GET api/values
        [HttpGet]
        [Route("api/ETH/Transaction/Transaction")]
        public async Task<object> Transaction(string transactionId, string password, bool watchOnley = false)
        {
            //RPCClient client = new RPCClient(Network.TestNet);
            var user = db.Users.SingleOrDefault(w => w.UserId == CurrentUser.UserId);
            if (user == null)
                throw new BusinessException("User not valid", 121);

            var walletFileName = string.Format(HttpContext.Current.Request.PhysicalApplicationPath + Config.DefaultWalletFileName, user.Email);
            var wallet = user.Wallets.SingleOrDefault(w => w.CurrencyType == CurrencyTypeEnum.ETH);
            if (wallet == null)
                throw new BusinessException("Wallet not valid", 121);

            var addressFrom = wallet.Address;

            //var safe = Safe.Load(user.Password, walletFileName);
            var privateKey = Key.Parse(user.Privatekey, password, Config.Network);

            //var key = new EthECKey(safe.BitcoinExtKey.PrivateKey.ToBytes(), true);
            var key = new EthECKey(privateKey.ToBytes(), true);

            var web3 = GetWeb3(key);
            var tx = await web3.Eth.Transactions.GetTransactionByHash.SendRequestAsync(transactionId);

            return new ObjectResultDto<EthrTxDto>
            {
                Result = new EthrTxDto
                {
                    from = tx.From,
                    hash = tx.TransactionHash,
                    blockHash = tx.BlockHash,
                    blockNumber = tx.BlockNumber.ToString(),
                    gasPrice = tx.GasPrice.ToString(),
                    gas = tx.Gas.ToString(),
                }
            };
        }

        // GET api/values
        [HttpGet]
        [Route("api/ETH/Transaction/Transactions")]
        public async Task<object> Transactions(string password,string account = null, int pageSize = 10, int from = 0, bool watchOnley = false)
        {
            //RPCClient client = new RPCClient(Network.TestNet);
            var user = db.Users.Single(w => w.UserId == CurrentUser.UserId);
            var wallet = user.Wallets.SingleOrDefault(w => w.CurrencyType == CurrencyTypeEnum.ETH);
            if (wallet == null)
                throw new BusinessException("Wallet not valid", 121);

            //var walletFileName = string.Format(HttpContext.Current.Request.PhysicalApplicationPath + Config.DefaultWalletFileName, user.Email);

            //var privateKey = Key.Parse(user.Privatekey, password, Config.Network);

            //var safe = Safe.Load(user.Password, walletFileName);
            
            //var key = new EthECKey(privateKey.ToBytes(), true);

            try
            {

                using (var wc = new WebClient())
                {
                    var s = wc.DownloadString("http://api-rinkeby.etherscan.io/api?module=account&action=txlist&address=" + wallet.Address + "&&sort=asc&apikey=YourApiKeyToken");

                    var r = JsonConvert.DeserializeObject<EthdataDto>(s);

                    return new ObjectResultDto<PagingResult<TranactionDto>>
                    {
                        Result = new PagingResult<TranactionDto>
                        {
                            Result = r == null ? new List<TranactionDto>() : r.result == null ? new List<TranactionDto>() : r.result.Select(txDto => new TranactionDto
                            {
                                Amount = (String.Equals(txDto.from, wallet.Address, StringComparison.CurrentCultureIgnoreCase) ? txDto.Value * -1 : txDto.Value) / 1000000000000000000,
                                TransactionId = txDto.hash,
                                Created = txDto.timeStamp,
                                Address = new List<string> { (String.Equals(txDto.from, wallet.Address, StringComparison.CurrentCultureIgnoreCase) ? txDto.to : txDto.from) }

                            }).OrderByDescending(o => o.Created).ToList()
                        }
                    };
                }

            }
            catch (Exception ex)
            {

                throw new BusinessException(ex.Message, 1245);
            }

        }

        // POST api/values
        [HttpPost]
        [Route("api/ETH/Transaction/SendTransaction")]
        public async Task<ObjectResultDto<TranactionDto>> SendTransaction([FromBody]TranactionSendDto dto)
        {
            //_gasPrice = Web3.Web3.Convert.FromWei(TransactionBase.DEFAULT_GAS_PRICE, UnitConversion.EthUnit.Gwei);

            var user = db.Users.SingleOrDefault(w => w.UserId == CurrentUser.UserId);
            if (user == null)
                throw new BusinessException("User not valid", 121);

            var walletFileName = string.Format(HttpContext.Current.Request.PhysicalApplicationPath + Config.DefaultWalletFileName, user.Email);
            var wallet = user.Wallets.SingleOrDefault(w => w.CurrencyType == CurrencyTypeEnum.ETH);
            if (wallet == null)
                throw new BusinessException("Wallet not valid", 121);

            var addressFrom = wallet.Address;

            var privateKey = Key.Parse(user.Privatekey, dto.Password, Config.Network);

            var key = new EthECKey(privateKey.ToBytes(), true);

            //var key = new EthECKey(safe.BitcoinExtKey.PrivateKey.ToBytes(), true);

            var web3 = GetWeb3(key);

            var transactionPolling = web3.TransactionManager.TransactionReceiptService;

            //var txCount = await web3.Eth.Transactions.GetTransactionCount.SendRequestAsync(addressFrom);

            var inputsTx = new List<TransactionInput>
            {
                new TransactionInput
                {
                    Value = new HexBigInteger(Web3.Convert.ToWei(dto.Amount)),
                    From = addressFrom,
                    To = dto.AddressTo
                }
            };

            try
            {
                var res = await transactionPolling.SendRequestsAndWaitForReceiptAsync(inputsTx);

                return new ObjectResultDto<TranactionDto>
                {
                    Result = new TranactionDto
                    {
                        TransactionId = res.FirstOrDefault()?.TransactionHash.ToString()
                    }
                };

                //return new ObjectResultDto<List<TransactionReceipt>>
                //{
                //    Result = res
                //};

            }
            catch (Exception)
            {

                throw new BusinessException("Invalid transaction");
            }

            //var transferFunction = new TransferFunction
            //{
            //    Value = Web3.Convert.ToWei(dto.Amount),
            //    FromAddress = addressFrom,
            //    To = dto.AddressTo
            //};

            //var res = await web3.Eth.GetContractHandler(dto.AddressTo).SendRequestAsync(transferFunction);

            //var transactionInput = new TransactionInput
            //{

            //};

            //var web31 = GetWeb3(transactionInput.From);
            //var r= await web31.TransactionManager.SendTransactionAsync(transactionInput);


        }

        private Web3 GetWeb3(EthECKey privateKey)
        {
            //var privateKey = _accountKeySecureStorageService.GetPrivateKey(accountAddress);
            if (privateKey == null) throw new Exception("Account not configured for signing transactions");
            //todo chainId
            return new Web3(new Account(privateKey), Config.ETHClientUrl);
        }
    }
}

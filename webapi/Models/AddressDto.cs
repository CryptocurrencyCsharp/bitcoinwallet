﻿
namespace webapi.Models
{
    public class AddressDto
    {
        public string Address { get; set; }

        public string CurrencyType { get; set; }

        public string Label { get; set; }

        public bool IsArchive { get; set; }
    }
}
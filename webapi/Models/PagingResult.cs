﻿using System.Collections.Generic;

namespace webapi.Models
{
    public class PagingResult<T> where T : class
    {
        public long TotalCount { get; set; }
        public List<T> Result { get; set; }

    }
}
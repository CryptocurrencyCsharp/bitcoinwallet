﻿namespace webapi.Models
{
    public static class CurrencyTypeEnum
    {
        public static string BTC
        {
            get { return "BTC"; }
        }
        public static string BTH
        {
            get { return "BTH"; }
        }

        public static string ETH
        {
            get { return "ETH"; }
        }

    }
}
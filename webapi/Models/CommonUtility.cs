﻿using System;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace webapi.Models
{

    public static class CommonUtility
    {
        private const string TripleDesKey = "1C1C1C1C1C1C1C1C";
        private static readonly byte[] Iv = { 0, 0, 0, 0, 0, 0, 0, 0 };

        public static int GenerateRandomNo()
        {
            var min = 1000;
            var max = 9999;
            var rdm = new Random();
            return rdm.Next(min, max);
        }

        public static int GenerateRandomNo(int min, int max)
        {
            var rdm = new Random();
            return rdm.Next(min, max);
        }

        public static int ToPage(int offset, int limit)
        {
            if (offset <= 0)
                return 1;

            if (limit <= 0)
                return 1;

            var page = offset / limit;

            return page + 1;
        }

        public static DateTime FromUnixTime(this long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddMilliseconds(unixTime);
        }


        public static long ToUnixTime(this DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalMilliseconds);
        }

        public static string GetTimeCode()
        {

            Thread.Sleep(1000);
            return Math.Abs(DateTime.Now.ToString("yyMMddhhmmss").GetHashCode()).ToString().Replace("-", "");
            //return Convert.ToInt32();
        }

        public static string GetHash(string plaintext)
        {

            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(plaintext);
            var hash = md5.ComputeHash(inputBytes);

            var sb = new StringBuilder();

            foreach (var t in hash)
            {
                sb.Append(t.ToString());
            }

            return sb.ToString();
        }

        public static string GetReferenceNumber()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }

        public static string CalculateMd5Hash(string input)

        {

            // step 1, calculate MD5 hash from input

            var md5 = MD5.Create();

            var inputBytes = Encoding.ASCII.GetBytes(input);

            var hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            var sb = new StringBuilder();

            foreach (var t in hash)
            {
                sb.Append(t.ToString("X2"));
            }

            return sb.ToString();

        }



        private static object SendClientRequest(string url, string loginjson, string contentType = "application/json")
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = contentType;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(loginjson);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            string result;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;

        }

        public static DbGeography CreatePoint(double latitude, double longitude)
        {
            var text = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({0} {1})", longitude, latitude);
            // 4326 is most common coordinate system used by GPS/Maps
            return DbGeography.PointFromText(text, 4326);
        }

        public static string GetDateMpg()
        {
            return DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');
        }

        public static string GetTimeMpg()
        {
            return DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0');
        }

        public static string GetPinBlock(string pan, string pin)
        {
            var bArray = CalculatePinBlock(pan, pan.Length, pin, pin.Length);
            return EncryptDes(bArray);
        }

        private static byte[] CalculatePinBlock(string pan, int panLen, string pin, int pinLen)
        {
            try
            {
                var code = new byte[2, 8];
                var index = panLen - 13; // 3; // PANLen - 13

                for (short i = 0; i < 8; i++)
                    code[0, i] = 255;

                code[0, 0] = (byte)pinLen;
                for (short i = 0; i < pinLen / 2; i++)
                    code[0, i + 1] = (byte)(((pin.ToCharArray()[2 * i] - '0') * 16) + (pin.ToCharArray()[(2 * i) + 1] - '0'));
                if ((pinLen % 2) != 0)
                    code[0, (pinLen / 2) + 1] = (byte)(((pin.ToCharArray()[pinLen - 1] - '0') * 16) + 15);

                code[1, 0] = 0;
                code[1, 1] = 0;
                code[1, 2] = (byte)((pan.ToCharArray()[index] - '0') * 16 + (pan.ToCharArray()[index + 1] - '0'));
                code[1, 3] = (byte)((pan.ToCharArray()[index + 2] - '0') * 16 + (pan.ToCharArray()[index + 3] - '0'));
                code[1, 4] = (byte)((pan.ToCharArray()[index + 4] - '0') * 16 + (pan.ToCharArray()[index + 5] - '0'));
                code[1, 5] = (byte)((pan.ToCharArray()[index + 6] - '0') * 16 + (pan.ToCharArray()[index + 7] - '0'));
                code[1, 6] = (byte)((pan.ToCharArray()[index + 8] - '0') * 16 + (pan.ToCharArray()[index + 9] - '0'));
                code[1, 7] = (byte)((pan.ToCharArray()[index + 10] - '0') * 16 + (pan.ToCharArray()[index + 11] - '0'));


                for (short counter = 0; counter < 8; counter++)
                    code[0, counter] = (byte)(code[0, counter] ^ code[1, counter]);

                var colByteArray = new byte[8];
                for (short counter = 0; counter < 8; counter++)
                {
                    colByteArray[counter] = code[0, counter];
                }
                return colByteArray;
            }
            catch (Exception)
            {
                return new byte[] { };
            }
        }

        private static string EncryptDes(byte[] toEncryptArray)
        {
            var keyArray = HexStringToByteArray(TripleDesKey);
            var objDes = new DESCryptoServiceProvider { Padding = PaddingMode.None };
            var objmst = new MemoryStream();
            var objcs = new CryptoStream(objmst, objDes.CreateEncryptor(keyArray, Iv), CryptoStreamMode.Write);
            objcs.Write(toEncryptArray, 0, toEncryptArray.Length);
            objcs.FlushFinalBlock();
            var hex1 = BitConverter.ToString(objmst.ToArray()).Replace("-", string.Empty);
            return hex1;
        }

        private static byte[] HexStringToByteArray(string hex)
        {
            hex = hex.PadLeft(16, '0');
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
}
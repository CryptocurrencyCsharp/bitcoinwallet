namespace webapi.Models
{
    public class WalletRestoreDto
    {
        public string mnomenic { get; set; }

        public string Email { get; set; }    

        public string Password{ get; set; }    
    }

    public class UserDto
    {
        public string Password { get; set; }

        public string WalletId { get; set; }
    }
}
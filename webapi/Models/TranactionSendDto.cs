namespace webapi.Controllers
{
    public class TranactionSendDto
    {
        public string AddressFrom { get; set; }

        public string AddressTo { get; set; }

        public string Amount { get; set; }

        public string Fee { get; set; }

        public string Password { get; set; }
    }
}
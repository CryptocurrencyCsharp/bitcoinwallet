using System.Collections.Generic;
using NBitcoin;
using Nethereum.Hex.HexTypes;
using Newtonsoft.Json;

namespace webapi.Models
{

    public class TransactionHistoryDto
    {
        public dataDto data { get; set; }
    }

    public class TransactionSummeryDto
    {
        public TxDto data { get; set; }
    }

    public class dataDto
    {
        public List<TxDto> list { get; set; }
    }
    public class EthdataDto
    {
        public List<EthrTxDto> result { get; set; }
    }

    public class TxDto
    {
        public string hash { get; set; }

        public long created_at { get; set; }

        public decimal balance_diff { get; set; }

        public List<TxInputDto> inputs { get; set; }

        public List<TxOutputDto> outputs { get; set; }

    }

    public class EthrTxDto
    {

        public string blockHash { get; set; }

        public string blockNumber { get; set; }

        public string input { get; set; }

        public string Nonce { get; set; }

        public string TransactionIndex { get; set; }

        public decimal Value { get; set; }

        public string from { get; set; }

        public string to { get; set; }

        public string gas { get; set; }

        public string gasPrice { get; set; }

        public string hash { get; set; }

        public long timeStamp { get; set; }

        public string isError { get; set; }

    }

    public class TxOutputDto
    {
        public List<string> addresses { get; set; }
    }
    public class TxInputDto
    {
        public List<string> prev_addresses { get; set; }
    }

    public class TranactionDto
    {

        public List<string> Address { get; set; }

        public decimal Amount { get; set; }

        public string TransactionId { get; set; }

        public long Created { get; set; }

        //public string Hash { get; set; }
        //public string Type { get; set; }
        //public string TimeStamp { get; set; }
        //public object Data { get; set; }

    }

    public class txrefs
    {
        public string tx_hash { get; set; }
        public string block_height { get; set; }

        public string tx_input_n { get; set; }

        public string tx_output_n { get; set; }

        public string value { get; set; }

        public string ref_balance { get; set; }
        
        //  "spent": false,
        //  "confirmations": 63066,
        //  "confirmed": "2014-05-22T03:46:25Z",
        //  "double_spend": false
    }
}
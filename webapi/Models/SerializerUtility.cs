﻿using System;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace webapi.Models
{
    public static class SerializerUtility
    {

        
        public static string Serialize(this object obj)
        {
            var sw = new StringWriter(new StringBuilder());

            var formatter = new LosFormatter();
            formatter.Serialize(sw, obj);

            return sw.ToString();
        }

        public static object Deserialize(this string data)
        {
            return data == null ? null : (new LosFormatter()).Deserialize(data);
        }

        public static string ObjectToXml(object obj, string rootName = "DataSet")
        {
            if (obj == null)
                return null;

            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            var xmlSerializer = new XmlSerializer(obj.GetType(), new XmlRootAttribute(rootName));

            using (var sw = new StringWriter())
            {
                xmlSerializer.Serialize(sw, obj, ns);
                return sw.ToString();
            }
        }

        public static T XmlToObject<T>(string xml) where T : class
        {
            if (string.IsNullOrEmpty(xml))
                return default(T);

            var serializer = new XmlSerializer(typeof(T));
            object result;

            try
            {
                using (TextReader reader = new StringReader(xml))
                {
                    result = serializer.Deserialize(reader);
                }
            }
            catch (Exception)
            {

                throw new Exception("اطلاعات درخواست نامعتبر است");
            }


            return (T)result;
        }
    }
}
﻿using System.Collections.Generic;

namespace webapi.Models
{
    public class ObjectResultDto<T> where T : class
    {
        public bool Success { get { return true; } }

        public int ErrorCode { get { return 0; } }

        public string ErrorMessage { get { return null; } }

        public T Result { get; set; }
    }

    public class ObjectResultDto
    {
        public bool Success { get { return true; } }

        public int ErrorCode { get { return 0; } }

        public string ErrorMessage { get { return null; } }

        public object Result { get { return null; } }
    }
}
using System;
using System.Collections;

namespace webapi.Models
{
    public class BusinessException : Exception
    {
        public BusinessException(string message)
        {
            ErrorMessage = message;
        }

        public BusinessException(string message, int errorCode = 10101) : base(message)
        {
            ErrorCode = errorCode;
        }

        public BusinessException(string message, int errorCode, IDictionary errorData) : base(message)
        {
            ErrorCode = errorCode;
            ErrorData = errorData;
        }


        public string ErrorMessage { get; set; }

        public int ErrorCode { get; set; }

        public object ErrorTitle { get; set; }

        public IDictionary ErrorData { get; set; }

    }
}

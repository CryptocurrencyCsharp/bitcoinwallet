using System.ComponentModel.DataAnnotations;

namespace webapi.Models
{
    public class UserSignUpDto
    {
        public string Password { get; set; }

        [EmailAddress(ErrorMessage = "Email is invalid")]
        public string Email { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using NBitcoin;
using QBitNinja.Client;
using webapi.Datamodel;

namespace webapi.ApplicationServices
{
    public class ApplicationWalletService : IApplicationWalletService
    {
        private static readonly QBitNinjaClient Client = new QBitNinjaClient(Network.Main);
        //private static readonly ICoinService CoinService = new BitcoinService(useTestnet: true);

        public void CreateNewAddress(int userId, string password)
        {
            var network = Network.TestNet;

            var privateKey = new Key();
            var bitcoinPrivateKey = privateKey.GetWif(network);
            var address = bitcoinPrivateKey.GetAddress();
            var bitcoinApiEntities = new BitcoinApiEntities();
            //bitcoinApiEntities.Wallets.
        }


        public decimal CheckBalance(BitcoinPubKeyAddress address)
        {
            var balanceModel = Client.GetBalance(address, true).Result;
            decimal balance = 0;

            if (balanceModel.Operations.Count > 0)
            {
                var unspentCoins = new List<Coin>();
                foreach (var operation in balanceModel.Operations)
                    unspentCoins.AddRange(operation.ReceivedCoins.Select(coin => coin as Coin));
                balance = unspentCoins.Sum(x => x.Amount.ToDecimal(MoneyUnit.BTC));
            }
            return balance;
        }
    }
}
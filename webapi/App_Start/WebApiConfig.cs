﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Web.Http;
using webapi.Attributes;
using webapi.Models;

namespace webapi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            log4net.Config.XmlConfigurator.Configure();
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi1",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi2",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Filters.Add(new ValidationFilterAttribute());

            config.Filters.Add(new UnhandledExceptionFilterAttribute()
                   .Register<SecurityException>(HttpStatusCode.Forbidden)
                   .Register<SecurityException>(HttpStatusCode.Forbidden)
                   .Register<UnauthorizedAccessException>((exception, request) =>
                   {
                       var businessException = exception as UnauthorizedAccessException;
                       var response = request.CreateResponse((HttpStatusCode)200, new
                       {
                           Success = false,
                           ErrorMessage = "دسترسی وجود ندارد",
                           Message = "دسترسی وجود ندارد",
                           ErrorCode = 10001
                       });
                       response.ReasonPhrase = "Unauthorized";

                       return response;
                   })
                   .Register<Exception>((exception, request) =>
                   {

                       var response = request.CreateResponse((HttpStatusCode)200, new
                       {
                           Success = false,
                           ErrorMessage = "خطای داخلی ",
                           Message = "خطای داخلی ",
                           ErrorCode = -505
                       });
                       response.ReasonPhrase = "innerException";

                       return response;
                   })
                   .Register<BusinessException>((exception, request) =>
                   {
                       var businessException = exception as BusinessException;
                       var response = request.CreateResponse((HttpStatusCode)200, new
                       {
                           businessException.ErrorCode,
                           Success = false,
                           ErrorMessage = string.IsNullOrEmpty(businessException.Message) ? businessException.ErrorMessage : businessException.Message,
                       });
                       response.ReasonPhrase = "business";

                       return response;
                   })
                   .Register<EntityCommandExecutionException>((exception, request) =>
                   {
                       try
                       {

                           var businessException = new BusinessException(exception.Message);

                           var response = request.CreateResponse((HttpStatusCode)550, new
                           {
                               businessException.ErrorCode,
                               Success = false,
                               ErrorMessage = string.IsNullOrEmpty(businessException.Message) ? businessException.ErrorMessage : businessException.Message,
                           });
                           response.ReasonPhrase = "businessException";

                           return response;

                       }
                       catch (Exception)
                       {
                       }

                       return request.CreateResponse((HttpStatusCode)550, new
                       {
                           ErrorCode = 550,
                           Success = false,
                           ErrorMessage = string.IsNullOrEmpty(exception.Message) ? "EntityCommandExecutionException" : exception.Message,
                       });

                   })
                   .Register<DbEntityValidationException>((exception, request) =>
                   {
                       try
                       {
                           var validationError = (exception as DbEntityValidationException).EntityValidationErrors.SelectMany(c => c.ValidationErrors).ToList();
                           var messages = validationError.Aggregate("", (current, dbValidationError) => current + " " + dbValidationError.ErrorMessage);


                           var response = request.CreateResponse((HttpStatusCode)550, new
                           {
                               ErrorCode = 10101,
                               Success = false,
                               ErrorMessage = messages,
                           });
                           return response;

                       }
                       catch (Exception)
                       {
                       }

                       return request.CreateResponse((HttpStatusCode)550, new
                       {
                           ErrorCode = 550,
                           Success = false,
                           ErrorMessage = string.IsNullOrEmpty(exception.Message) ? "EntityCommandExecutionException" : exception.Message,
                       });

                   })

                   .Register<SqlException>((exception, request) =>
                   {
                       var sqlException = exception as SqlException;
                       if (sqlException?.Number >= 50000)
                       {

                           //var ex = SerializerUtility.XmlToObject<ExceptionDto>(sqlException.Message);

                           //return request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message.Replace(Environment.NewLine, String.Empty));
                       }

                       return request.CreateResponse(HttpStatusCode.InternalServerError, new
                       {
                           ErrorCode = 555,
                           Success = false,
                           ErrorMessage = string.IsNullOrEmpty(sqlException.Message) ? "EntityCommandExecutionException" : sqlException.Message,
                       });
                   }
                   )
               );
        }
    }
}

﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace webapi.Attributes
{
    public class RequireAuthenticationAttribute : AuthorizeAttribute
    {
        private readonly int _securityKey;

        public RequireAuthenticationAttribute()
        {
            _securityKey = -1;
        }

        private UserPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as UserPrincipal; }
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            if (CurrentUser == null) throw new UnauthorizedAccessException();

            if (CurrentUser != null && _securityKey == -1)
            {
                
                return true;
            }
            return CurrentUser != null;
            //&& CurrentUser.FkPermissions.Any(permissionId => permissionId == _securityKey);
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
        }
    }
}
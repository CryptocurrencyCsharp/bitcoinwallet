﻿using System;
using System.Security.Principal;

namespace webapi.Attributes
{
    public class UserPrincipal : IPrincipal
    {
        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }

        public IIdentity Identity { get; private set; }

        public UserPrincipal(string userName)
        {
            Identity = new GenericIdentity(userName);
        }

        public int UserId { get; set; }

        public string Token { get; set; }

    }
}